﻿using UnityEngine;

public class BeeMovement : MonoBehaviour
{
    public float speed = 5f; // ความเร็วในการเคลื่อนที่ของผึ้ง
    public Collider areaZone; // พื้นที่ที่ผึ้งสามารถเคลื่อนที่ได้

    private Vector3 targetPosition;

    void Start()
    {
        if (areaZone == null)
        {
            Debug.LogError("AreaZone is not set. Please set it in the inspector.");
            return;
        }

        // กำหนดจุดเป้าหมายเริ่มต้นให้เป็นตำแหน่งปัจจุบันของผึ้ง
        targetPosition = transform.position;
    }

    void Update()
    {
        // ถ้าผึ้งถึงจุดเป้าหมาย กำหนดจุดเป้าหมายใหม่ภายในพื้นที่ที่กำหนด
        if (transform.position == targetPosition)
        {
            targetPosition = new Vector3(
                Random.Range(areaZone.bounds.min.x, areaZone.bounds.max.x),
                Random.Range(areaZone.bounds.min.y, areaZone.bounds.max.y),
                Random.Range(areaZone.bounds.min.z, areaZone.bounds.max.z)
            );
        }

        // ขยับผึ้งไปยังจุดเป้าหมาย
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
    }
}
