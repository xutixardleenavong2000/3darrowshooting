﻿using System.Collections;
using UnityEngine;

public class ArrowCameraHit : MonoBehaviour
{
    public Camera mainCamera;
    public float moveSpeed = 10f; // ความเร็วในการเคลื่อนที่ของกล้อง
    public float rotateSpeed = 10f; // ความเร็วในการหมุนของกล้อง
    private Vector3 originalPosition;
    private Quaternion originalRotation;
    private bool isMoving = false;
    public float minDistance = 5f; // ระยะที่กล้องจะหยุดเคลื่อนที่
    public float safeDistance = 5f; // ระยะที่จะใช้เพื่อให้กล้องไม่ใกล้เกินไปกับวัตถุปลายทาง

    void Start()
    {
        if (mainCamera == null)
        {
            mainCamera = Camera.main;
        }
        originalPosition = mainCamera.transform.position;
        originalRotation = mainCamera.transform.rotation;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Zero")
        {
            // ถ้าวัตถุที่ชนมีแท็ก "Zero" ก็ไม่ต้องทำอะไร
            return;
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("Target") && !isMoving)
        {
            Vector3 contactPoint = collision.contacts[0].point;
            float distanceToTarget = Vector3.Distance(mainCamera.transform.position, contactPoint);
            if (distanceToTarget > minDistance + safeDistance)
            {
                StartCoroutine(MoveCamera(contactPoint));
            }
        }
    }


    IEnumerator MoveCamera(Vector3 targetPosition)
    {
        isMoving = true;

        while (Vector3.Distance(mainCamera.transform.position, targetPosition) > minDistance)
        {
            // เคลื่อนที่กล้องไปที่จุดเป้าหมาย
            mainCamera.transform.position = Vector3.MoveTowards(mainCamera.transform.position, targetPosition, moveSpeed * Time.deltaTime);

            // หมุนกล้องให้เรียกมองที่จุดเป้าหมาย
            Vector3 targetDirection = targetPosition - mainCamera.transform.position;
            Quaternion targetRotation = Quaternion.LookRotation(targetDirection);
            mainCamera.transform.rotation = Quaternion.Slerp(mainCamera.transform.rotation, targetRotation, rotateSpeed * Time.deltaTime);

            yield return null;
        }

        yield return new WaitForSeconds(1.0f); // รอ 1 วินาที ก่อนที่จะรีเซ็ตกล้อง

        StartCoroutine(ResetCamera());
    }

    IEnumerator ResetCamera()
    {
        isMoving = false;

        while (Vector3.Distance(mainCamera.transform.position, originalPosition) > 0.1f || Quaternion.Angle(mainCamera.transform.rotation, originalRotation) > 0.1f)
        {
            // เคลื่อนที่กล้องไปที่ตำแหน่งเริ่มต้น
            mainCamera.transform.position = Vector3.MoveTowards(mainCamera.transform.position, originalPosition, moveSpeed * Time.deltaTime);

            // หมุนกล้องให้ชี้ที่ทิศทางเริ่มต้น
            mainCamera.transform.rotation = Quaternion.RotateTowards(mainCamera.transform.rotation, originalRotation, rotateSpeed * Time.deltaTime);

            yield return null;
        }

        // รีเซ็ตตำแหน่งและการหมุนของกล้องเป็นตำแหน่งเริ่มต้น
        mainCamera.transform.position = originalPosition;
        mainCamera.transform.rotation = originalRotation;
    }
}
