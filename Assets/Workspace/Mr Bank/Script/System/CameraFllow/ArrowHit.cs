﻿using System.Collections;
using UnityEngine;

public class ArrowHit : MonoBehaviour
{
    public Camera mainCamera;
    public float moveSpeed = 5f; // ความเร็วในการเคลื่อนที่ของกล้อง
    public float rotateSpeed = 5f; // ความเร็วในการหมุนของกล้อง
    private Vector3 originalPosition;
    private Quaternion originalRotation;
    private bool isMoving = false;

    void Start()
    {
        originalPosition = mainCamera.transform.position;
        originalRotation = mainCamera.transform.rotation;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Target") && !isMoving)
        {
            StartCoroutine(MoveCamera(collision.contacts[0].point));
        }
    }

    IEnumerator MoveCamera(Vector3 targetPosition)
    {
        isMoving = true;
        float minDistance = 5f; // ระยะที่กล้องจะหยุดเคลื่อนที่

        while (Vector3.Distance(mainCamera.transform.position, targetPosition) > minDistance)
        {
            // เคลื่อนที่กล้องไปที่จุดเป้าหมาย
            mainCamera.transform.position = Vector3.MoveTowards(mainCamera.transform.position, targetPosition, moveSpeed * Time.deltaTime);

            // หมุนกล้องให้เรียกมองที่จุดเป้าหมาย
            Vector3 targetDirection = targetPosition - mainCamera.transform.position;
            Quaternion targetRotation = Quaternion.LookRotation(targetDirection);
            mainCamera.transform.rotation = Quaternion.RotateTowards(mainCamera.transform.rotation, targetRotation, rotateSpeed * Time.deltaTime);

            yield return null;
        }

        yield return new WaitForSeconds(1.0f); // รอ 1 วินาที ก่อนที่จะรีเซ็ตกล้อง

        StartCoroutine(ResetCamera());
    }

    IEnumerator ResetCamera()
    {
        isMoving = false;

        while (Vector3.Distance(mainCamera.transform.position, originalPosition) > 2f || Quaternion.Angle(mainCamera.transform.rotation, originalRotation) > 2f)
        {
            // เคลื่อนที่กล้องไปที่ตำแหน่งเริ่มต้น
            mainCamera.transform.position = Vector3.MoveTowards(mainCamera.transform.position, originalPosition, moveSpeed * Time.deltaTime);

            // หมุนกล้องให้ชี้ที่ทิศทางเริ่มต้น
            mainCamera.transform.rotation = Quaternion.RotateTowards(mainCamera.transform.rotation, originalRotation, rotateSpeed * Time.deltaTime);

            yield return null;
        }

        // รีเซ็ตตำแหน่งและการหมุนของกล้องเป็นตำแหน่งเริ่มต้น
        mainCamera.transform.position = originalPosition;
        mainCamera.transform.rotation = originalRotation;
    }
}