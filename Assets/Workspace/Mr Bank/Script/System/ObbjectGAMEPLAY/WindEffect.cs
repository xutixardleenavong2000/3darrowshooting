using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindEffect : MonoBehaviour
{
    public float windStrength = 10f; 
    public Vector3 windDirection = new Vector3(1, 0, 0); 

    void OnTriggerStay(Collider other)
    {
        Rigidbody rb = other.GetComponent<Rigidbody>();
        if (rb != null)
        {
            rb.AddForce(windDirection * windStrength * Time.deltaTime, ForceMode.Force);
        }
    }
}
