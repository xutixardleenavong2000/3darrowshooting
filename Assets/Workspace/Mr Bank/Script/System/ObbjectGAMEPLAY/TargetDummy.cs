﻿using UnityEngine;

public class TargetDummy : MonoBehaviour
{
    public float moveSpeed = 2f; // ความเร็วในการเคลื่อนที่
    public float moveRange = 2f; // ระยะทางการเคลื่อนที่
    public GameObject[] linkedObjects; // วัตถุที่เชื่อมโยง

    private Vector3 initialPosition; // ตำแหน่งเริ่มต้น
    private bool isMoving = true; // ตัวแปรที่ควบคุมว่าวัตถุควรเคลื่อนที่หรือไม่

    void Start()
    {
        initialPosition = transform.position; // บันทึกตำแหน่งเริ่มต้น
    }

    void Update()
    {
        if (isMoving)
        {
            // คำนวณการเคลื่อนที่โดยใช้ฟังก์ชัน Sinusoidal
            float moveAmount = Mathf.Sin(Time.time) * moveRange;

            // ปรับตำแหน่งตามการเคลื่อนที่
            transform.position = initialPosition + new Vector3(moveAmount, 0f, 0f) * moveSpeed;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Arrow")
        {
            // หยุดการเคลื่อนที่เมื่อถูกชนโดย Arrow
            isMoving = false;

            // หยุดการเคลื่อนที่ของวัตถุที่เชื่อมโยง
            foreach (GameObject linkedObject in linkedObjects)
            {
                linkedObject.GetComponent<TargetDummy>().isMoving = false;
            }

            // หยุดการเคลื่อนที่หลังจาก 3.5 วินาที
            Invoke("ResumeMoving", 3.5f);
        }
    }

    void ResumeMoving()
    {
        // เริ่มการเคลื่อนที่อีกครั้ง
        isMoving = true;

        // เริ่มการเคลื่อนที่ของวัตถุที่เชื่อมโยง
        foreach (GameObject linkedObject in linkedObjects)
        {
            linkedObject.GetComponent<TargetDummy>().isMoving = true;
        }
    }
}

