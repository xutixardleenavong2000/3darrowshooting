﻿using System.Collections;
using UnityEngine;

public class TargetSpawner : MonoBehaviour
{
    public GameObject targetObject; // Object ที่จะ Spawn
    public GameObject spawnPlane; // Plane ที่ใช้เป็นขอบเขตในการสุ่ม Spawn
    private Bounds spawnBounds; // ขอบเขตในการสุ่ม Spawn

    void Start()
    {
        if (spawnPlane != null)
        {
            Renderer renderer = spawnPlane.GetComponent<Renderer>();
            spawnBounds = renderer.bounds; // รับขอบเขตของ Plane
        }
        else
        {
            Debug.LogError("Please assign the spawnPlane in the Inspector.");
        }

        StartCoroutine(SpawnObjectRoutine());
    }

    IEnumerator SpawnObjectRoutine()
    {
        while (true)
        {
            GameObject spawnedObject = SpawnObject();
            yield return new WaitForSeconds(1.5f); // รอ 1 วินาที
            Destroy(spawnedObject); // ทำลาย Object ที่ Spawn มาแล้ว
            yield return new WaitForSeconds(2f); // รอ 0.5 วินาทีเพื่อหยุดการทำงานของ Coroutine นี้
        }
    }

    GameObject SpawnObject()
    {
        Vector3 randomPosition = new Vector3(
            Random.Range(spawnBounds.min.x, spawnBounds.max.x),
            0,
            Random.Range(spawnBounds.min.z, spawnBounds.max.z)
        );

        GameObject spawnedObject = Instantiate(targetObject, randomPosition, Quaternion.identity); // สร้าง Object ในตำแหน่งที่สุ่มได้
        return spawnedObject;
    }
}