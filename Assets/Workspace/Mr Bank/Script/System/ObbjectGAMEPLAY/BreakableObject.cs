﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; // สำหรับ Text Legacy

public class BreakableObject : MonoBehaviour
{
    public ParticleSystem destroyEffect; // Effect ที่จะเล่นเมื่อวัตถุถูกทำลาย
    //public Text damageText; // Text UI ที่จะแสดงเมื่อวัตถุถูกทำลาย

    void OnCollisionEnter(Collision collision)
    {
        // ตรวจสอบว่าวัตถุที่ชนเป็นลูกธนูหรือไม่
        if (collision.gameObject.GetComponent<Arrow>() != null)
        {
            Instantiate(destroyEffect, transform.position, Quaternion.identity);
            Destroy(gameObject, 2f); // Destroy the object with a delay of 0 seconds
            Destroy(collision.gameObject, 2f); // Destroy the arrow
        }
    }
}
