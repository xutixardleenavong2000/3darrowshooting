﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; // สำหรับ Slider
using TMPro; // สำหรับ TextMeshPro

public class Singleplayer : MonoBehaviour
{
    public GameObject arrowPrefab;
    public Transform arrowSpawn;
    public float arrowSpeed = 53f;
    public float maxChargeTime = 5.0f;
    private float currentChargeTime = 0f;
    public ParticleSystem arrowTrail;
    public Camera cam;
    public Slider chargeSlider;
    public int arrowsPerPlayer = 10;
    private int currentPlayerArrows;
    public float fireDelay = 1.0f;
    private float lastFireTime;

    void Start()
    {
        if (arrowPrefab == null || arrowSpawn == null)
        {
            Debug.LogError("Please assign arrowPrefab and arrowSpawn in the Inspector.");
        }
        currentPlayerArrows = arrowsPerPlayer;
    }

    void Update()
    {
        if (currentPlayerArrows <= 0)
        {
            // Handle no arrows left scenario
            return;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            currentChargeTime = 0f;
        }

        if (Input.GetButton("Fire1"))
        {
            currentChargeTime += Time.deltaTime;
            UpdateChargeUI();
        }

        if (Input.GetButtonUp("Fire1") && Time.time >= lastFireTime + fireDelay)
        {
            Fire();
            lastFireTime = Time.time;
        }
    }

    void UpdateChargeUI()
    {
        float charge = Mathf.Clamp(currentChargeTime / maxChargeTime, 0f, 1f);
        chargeSlider.value = charge;
    }

    void Fire()
    {
        currentPlayerArrows--;
        if (currentChargeTime <= 0)
            return;

        if (arrowPrefab != null && arrowSpawn != null)
        {
            GameObject arrow = Instantiate(arrowPrefab, arrowSpawn.position, arrowSpawn.rotation);
            Rigidbody arrowRb = arrow.GetComponent<Rigidbody>();
            arrowRb.centerOfMass = new Vector3(0, 0, -0.5f);

            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Vector3 target;
            if (Physics.Raycast(ray, out hit))
            {
                target = hit.point;
            }
            else
            {
                target = ray.GetPoint(1000);
            }
            Vector3 direction = (target - arrow.transform.position).normalized;

            float charge = currentChargeTime / maxChargeTime;
            float currentSpeed = arrowSpeed * charge;
            arrowRb.velocity = direction * currentSpeed;

            ParticleSystem trail = Instantiate(arrowTrail, arrow.transform.position, Quaternion.identity);
            trail.transform.parent = arrow.transform;

            Destroy(arrow, 33.0f);
        }
    }
}