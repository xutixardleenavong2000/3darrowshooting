﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowMagic : MonoBehaviour
{
    private Rigidbody rb;
    public ParticleSystem collisionParticle; // ระบุ Particle ที่ต้องการเล่นเมื่อลูกศรชน

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if (rb.velocity.magnitude > 0.1f)
        {
            // ปรับให้ลูกศรหันไปทางทิศทางที่เคลื่อนที่
            transform.forward = rb.velocity.normalized;

            // อัปเดตตำแหน่งกล้องให้ติดตามลูกศร
        }
    }

    // เมื่อลูกศรชนกับวัตถุอื่น
    private void OnCollisionEnter(Collision collision)
    {
        // เล่น Particle
        if (collisionParticle != null)
        {
            Instantiate(collisionParticle, transform.position, Quaternion.identity);
        }
    }
}
