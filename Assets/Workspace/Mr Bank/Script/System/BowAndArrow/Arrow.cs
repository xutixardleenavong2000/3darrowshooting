﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    private Rigidbody rb;


    void Start()
    {
        rb = GetComponent<Rigidbody>();

    }


    void FixedUpdate()
    {
        if (rb.velocity.magnitude > 0.1f)
        {
            // ปรับให้ลูกศรหันไปทางทิศทางที่เคลื่อนที่
            transform.forward = rb.velocity.normalized;

            // อัปเดตตำแหน่งกล้องให้ติดตามลูกศร

        }
    }
}