using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MultiscoreManager : MonoBehaviour
{
    public TextMeshProUGUI[] scoreTextPlayer1;
    public TextMeshProUGUI[] scoreTextPlayer2;
    public GameObject gameWinUI;
    public TextMeshProUGUI scoreResultText;
    public AudioClip victorySound;

    public int maxRounds = 3;

    private int[] roundScoresPlayer1;
    private int[] roundScoresPlayer2;
    private int currentRound = 0;
    private int currentPlayer = 1;
    private bool checkedWinner = false;
    private int totalScorePlayer1 = 0;
    private int totalScorePlayer2 = 0;

    private AudioSource audioSource; 

    void Start()
    {
        InitializeScores();
        UpdateScoreUI();
        audioSource = GetComponent<AudioSource>(); 
    }

    void InitializeScores()
    {
        roundScoresPlayer1 = new int[maxRounds];
        roundScoresPlayer2 = new int[maxRounds];
    }

    public void UpdateScore(int playerID, int points)
    {
        if (currentRound < maxRounds * 2) 
        {
            if (playerID == 1)
            {
                roundScoresPlayer1[currentRound] += points;
                totalScorePlayer1 += points;
                UpdatePlayerScore(scoreTextPlayer1[currentRound], roundScoresPlayer1[currentRound]);
            }
            else if (playerID == 2)
            {
                if (currentRound - maxRounds >= 0 && currentRound - maxRounds < maxRounds)
                {
                    roundScoresPlayer2[currentRound - maxRounds] += points;
                    totalScorePlayer2 += points;
                    UpdatePlayerScore(scoreTextPlayer2[currentRound - maxRounds], roundScoresPlayer2[currentRound - maxRounds]);
                }
                else
                {
                    Debug.LogError("Attempting to access invalid index in roundScoresPlayer2 array.");
                    return; 
                }
            }

            UpdateScoreUI();

            if (++currentRound >= maxRounds * 2 && currentRound % (maxRounds * 2) == 0)
            {
                currentRound = 0;
                SwitchPlayer();
                if (checkedWinner == false)
                {
                    ShowResult();
                }
            }
        }
    }

    void UpdateScoreUI()
    {
        for (int i = 0; i < maxRounds; i++)
        {
            UpdatePlayerScore(scoreTextPlayer1[i], roundScoresPlayer1[i]);
            UpdatePlayerScore(scoreTextPlayer2[i], roundScoresPlayer2[i]);
        }
    }

    void UpdatePlayerScore(TextMeshProUGUI text, int score)
    {
        text.text = score.ToString();
    }

    void SwitchPlayer()
    {
        currentPlayer = (currentPlayer == 1) ? 2 : 1;
    }

    private void ShowResult()
    {
        checkedWinner = true;
        StartCoroutine(ShowResultWithDelay());
    }

    private IEnumerator ShowResultWithDelay()
    {
        yield return new WaitForSeconds(3f);
        gameWinUI.SetActive(true);

        if (totalScorePlayer1 > totalScorePlayer2)
        {
            scoreResultText.text = "Winner: Player 1\nScore: " + totalScorePlayer1.ToString();
        }
        else if (totalScorePlayer2 > totalScorePlayer1)
        {
            scoreResultText.text = "Winner: Player 2\nScore: " + totalScorePlayer2.ToString();
        }
        else
        {
            scoreResultText.text = "It's Draw!\nScore: " + totalScorePlayer1.ToString();
        }

        if (victorySound != null && audioSource != null)
        {
            audioSource.clip = victorySound;
            audioSource.Play();
        }
    }
}
