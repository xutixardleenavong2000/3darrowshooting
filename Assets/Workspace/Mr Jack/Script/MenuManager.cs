using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public GameObject startMenuPanel;
    public GameObject gameModePanel;
    public GameObject selectLevel1Panel;
    public GameObject selectLevel2Panel;
    public GameObject audioSettingPanel;
    public AudioSource buttonClickSound;

    private GameObject currentPanel;
    private bool isPaused = false;
    public string startMenuSceneName = "StartMenu";

    private void Start()
    {
        startMenuPanel.SetActive(true);
        gameModePanel.SetActive(false);
        selectLevel1Panel.SetActive(false);
        selectLevel2Panel.SetActive(false);
        audioSettingPanel.SetActive(false); 
        currentPanel = startMenuPanel;
    }

    public void Play()
    {
        startMenuPanel.SetActive(false);
        gameModePanel.SetActive(true);
        currentPanel = gameModePanel;
        buttonClickSound.Play();
    }

    public void Practice()
    {
        SceneManager.LoadScene("Training");
        buttonClickSound.Play();
    }

    public void Competition()
    {
        gameModePanel.SetActive(false);
        selectLevel1Panel.SetActive(true);
        currentPanel = selectLevel2Panel;
        buttonClickSound.Play();
    }
    public void MiniGame()
    {
        gameModePanel.SetActive(false);
        selectLevel2Panel.SetActive(true);
        currentPanel = selectLevel2Panel;
        buttonClickSound.Play();
    }

    public void Exit()
    {
        Application.Quit();
        buttonClickSound.Play();
    }

    public void OpenAudioSettings()
    {
        currentPanel.SetActive(false);
        audioSettingPanel.SetActive(true);
        currentPanel = audioSettingPanel;
        buttonClickSound.Play();
    }

    public void Close()
    {
        currentPanel.SetActive(false);
        if (currentPanel == audioSettingPanel)
        {
            startMenuPanel.SetActive(true);
            currentPanel = startMenuPanel;
        }
        else if (currentPanel == gameModePanel)
        {
            startMenuPanel.SetActive(true);
            currentPanel = startMenuPanel;
        }
        else if (currentPanel == selectLevel2Panel)
        {
            gameModePanel.SetActive(true);
            currentPanel = gameModePanel;
        }
        else if (currentPanel == selectLevel2Panel)
        {
            gameModePanel.SetActive(true);
            currentPanel = gameModePanel;
        }
        buttonClickSound.Play();
    }
    public void LoadTraning()
    {
        SceneManager.LoadScene("Traning");
        buttonClickSound.Play();
    }
    public void LoadLevel1()
    {
        SceneManager.LoadScene("Level1");
        buttonClickSound.Play();
    }
    public void LoadLevel2()
    {
        SceneManager.LoadScene("Level2");
        buttonClickSound.Play();
    }
    public void LoadLevel3()
    {
        SceneManager.LoadScene("Level3");
        buttonClickSound.Play();
    }
    public void LoadLevel4()
    {
        SceneManager.LoadScene("Level4");
        buttonClickSound.Play();
    }
    public void LoadLevelOne()
    {
        SceneManager.LoadScene("LevelOne");
        buttonClickSound.Play();
    }
    public void LoadLevelTwo()
    {
        SceneManager.LoadScene("LevelTwo");
        buttonClickSound.Play();
    }
}
