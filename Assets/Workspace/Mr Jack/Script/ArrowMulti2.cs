﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ArrowMulti2 : MonoBehaviour
{
    public WindEffect windEffect;
    private Rigidbody rb;
    public MultiscoreManager multiscoreManager;
    private bool hasHit = false;
    public AudioSource arrowStopSound;

    public AudioSource Playzero;
    public AudioSource PlayNormal;
    public AudioSource PlayGreat;

    private Transform target;

    // Array of raw images for each target
    public Sprite[] targetImages;

    public Image imageComponent;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        // Disable the image at the start
        imageComponent.enabled = false;
    }

    void FixedUpdate()
    {
        if (!hasHit)
        {
            rb.AddForce(windEffect.windDirection * windEffect.windStrength);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (!hasHit && collision.gameObject.layer == LayerMask.NameToLayer("Target"))
        {
            int points = 0;
            switch (collision.gameObject.tag)
            {
                case "Zero":
                    points = 0;
                    PlayZeroScoreSound();
                    break;
                case "White1":
                    PlayNormalScoreSound();
                    points = 1;
                    break;
                case "black":
                    PlayNormalScoreSound();
                    points = 2;
                    break;
                case "blue":
                    PlayNormalScoreSound();
                    points = 3;
                    break;
                case "red1":
                    PlayNormalScoreSound();
                    points = 4;
                    break;
                case "yellow":
                    PlayNormalScoreSound();
                    points = 6;
                    break;
                case "red2":
                    PlayGreatScoreSound();
                    points = 10;
                    break;
            }
            multiscoreManager.UpdateScore(2, points);
            hasHit = true;
            StopArrow();

            // Start a Coroutine to show the image after a delay
            StartCoroutine(ShowImageAndCloseAfterDelay(0.75f, collision.gameObject.tag));
        }
    }

    void PlayZeroScoreSound()
    {
        if (Playzero != null)
        {
            Playzero.Play();
        }

    }
    void PlayNormalScoreSound()
    {
        if (PlayNormal != null)
        {
            PlayNormal.Play();
        }

    }

    void PlayGreatScoreSound()
    {
        if (PlayGreat != null)
        {
            PlayGreat.Play();
        }

    }

    IEnumerator ShowImageAndCloseAfterDelay(float delay, string tag)
    {
        yield return new WaitForSeconds(delay);

        // Enable the image after the specified delay
        imageComponent.enabled = true;

        target = GameObject.FindGameObjectWithTag(tag).transform;
        transform.SetParent(target);

        // Display the appropriate image based on the collision with the target
        switch (tag)
        {
            case "Zero":
                imageComponent.sprite = targetImages[0];
                break;
            case "White1":
                imageComponent.sprite = targetImages[1];
                break;
            case "black":
                imageComponent.sprite = targetImages[2];
                break;
            case "blue":
                imageComponent.sprite = targetImages[3];
                break;
            case "red1":
                imageComponent.sprite = targetImages[4];
                break;
            case "yellow":
                imageComponent.sprite = targetImages[5];
                break;
            case "red2":
                imageComponent.sprite = targetImages[6];
                break;
        }

        // Close the image after another 2 seconds
        yield return new WaitForSeconds(2f);

        // Disable the image after the specified delay
        imageComponent.enabled = false;
    }

    void StopArrow()
    {
        rb.velocity = Vector3.zero;
        rb.isKinematic = true;
        arrowStopSound.Play();
    }
}
