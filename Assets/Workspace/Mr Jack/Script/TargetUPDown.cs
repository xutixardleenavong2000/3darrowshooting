﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetUpDown : MonoBehaviour
{
    public float moveSpeed = 2f;
    public float moveRange = 2f;
    public GameObject[] linkedObjects; // วัตถุที่เชื่อมโยง

    private Vector3 initialPosition;
    private bool isMoving = true;

    void Start()
    {
        initialPosition = transform.position;
    }

    void Update()
    {
        if (isMoving)
        {

            float moveAmount = Mathf.Sin(Time.time) * moveRange;


            transform.position = initialPosition + new Vector3(0f, moveAmount, 0f) * moveSpeed;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Arrow")
        {

            isMoving = false;

            // หยุดการเคลื่อนที่ของวัตถุที่เชื่อมโยง
            foreach (GameObject linkedObject in linkedObjects)
            {
                linkedObject.GetComponent<TargetUpDown>().isMoving = false;
            }

            // หยุดการเคลื่อนที่หลังจาก 3.5 วินาที
            Invoke("ResumeMoving", 3.5f);
        }
    }

    void ResumeMoving()
    {
        // เริ่มการเคลื่อนที่อีกครั้ง
        isMoving = true;

        // เริ่มการเคลื่อนที่ของวัตถุที่เชื่อมโยง
        foreach (GameObject linkedObject in linkedObjects)
        {
            linkedObject.GetComponent<TargetUpDown>().isMoving = true;
        }
    }
}
