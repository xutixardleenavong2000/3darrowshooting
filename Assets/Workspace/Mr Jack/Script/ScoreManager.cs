﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public TextMeshProUGUI[] scoreTextSinglePerson;
    public GameObject gameWinUI;
    public TextMeshProUGUI scoreResultText;
    public AudioClip victorySound;

    public int maxRounds = 3;

    private int[] roundScoresSinglePerson;
    private int currentRound = 0;
    private bool checkedWinner = false;
    private int totalScoreSinglePerson = 0;

    private AudioSource audioSource;

    void Start()
    {
        InitializeScores();
        UpdateScoreUI();
        audioSource = GetComponent<AudioSource>();
    }

    void InitializeScores()
    {
        roundScoresSinglePerson = new int[maxRounds];
    }

    public void UpdateScore(int playerID, int points)
    {
        if (currentRound < maxRounds)
        {
            if (playerID == 0)
            {
                roundScoresSinglePerson[currentRound] += points;
                totalScoreSinglePerson += points;
                UpdatePlayerScore(scoreTextSinglePerson[currentRound], roundScoresSinglePerson[currentRound]);
            }
            else
            {
                Debug.LogError("Invalid player ID!");
                return;
            }

            UpdateScoreUI();

            if (++currentRound >= maxRounds)
            {
                currentRound = 0;
                if (checkedWinner == false)
                {
                    ShowResult();
                }
            }
        }
    }

    void UpdateScoreUI()
    {
        for (int i = 0; i < maxRounds; i++)
        {
            UpdatePlayerScore(scoreTextSinglePerson[i], roundScoresSinglePerson[i]);
        }
    }

    void UpdatePlayerScore(TextMeshProUGUI text, int score)
    {
        text.text = score.ToString();
    }

    private void ShowResult()
    {
        checkedWinner = true;
        StartCoroutine(ShowResultWithDelay());
    }

    private IEnumerator ShowResultWithDelay()
    {
        yield return new WaitForSeconds(3f); // Delay of 3 seconds
        gameWinUI.SetActive(true);

        scoreResultText.text = "YourScore: " + totalScoreSinglePerson.ToString();

        if (victorySound != null && audioSource != null)
        {
            audioSource.clip = victorySound;
            audioSource.Play();
        }
    }
}