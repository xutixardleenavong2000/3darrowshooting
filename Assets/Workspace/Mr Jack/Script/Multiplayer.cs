﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class Multiplayer : MonoBehaviour
{
    public GameObject arrowPrefab;
    public Transform arrowSpawn;
    public float arrowSpeed = 100f;
    public float maxChargeTime = 5.0f;
    private float currentChargeTime = 0f;
    public ParticleSystem arrowTrail;
    public Camera cam;
    public Slider chargeSlider;
    public int arrowsPerPlayer = 3;
    public int player1Arrows = 3; // จำนวนลูกธนูของ Player 1
    public int player2Arrows = 3; // จำนวนลูกธนูของ Player 2
    private int currentPlayerArrows;
    public int currentPlayerID = 1;
    private bool isFirstFire = true;
    public float fireDelay = 5f;
    private float lastFireTime;

    public GameObject player1Image;
    public GameObject player2Image;
    public float displayTime = 3f;

    public GameObject arrowPrefabPlayer1;
    public GameObject arrowPrefabPlayer2;

    public TextMeshProUGUI player1ArrowsText; // เพิ่ม TextMeshProUGUI สำหรับแสดงจำนวนลูกธนูของ Player 1
    public TextMeshProUGUI player2ArrowsText; // เพิ่ม TextMeshProUGUI สำหรับแสดงจำนวนลูกธนูของ Player 2

    void Start()
    {
        if (arrowPrefab == null || arrowSpawn == null)
        {
            Debug.LogError("Please assign arrowPrefab and arrowSpawn in the Inspector.");
        }
        currentPlayerArrows = arrowsPerPlayer;

        player1Image.SetActive(true);
        player2Image.SetActive(false);

        // เริ่มต้นการแสดงผล UI และเลิกแสดงหลังจากเวลาที่กำหนด
        StartCoroutine(HidePlayerImage());

        // แสดงจำนวนลูกธนูเริ่มต้น
        UpdatePlayerArrowsText();
    }

    void Update()
    {
        if (currentPlayerArrows <= 0)
        {
            SwitchPlayerTurn();
        }

        if (Input.GetButtonDown("Fire1") && currentPlayerArrows > 0)
        {
            currentChargeTime = 0f;
        }

        if (Input.GetButton("Fire1") && currentPlayerArrows > 0)
        {
            currentChargeTime += Time.deltaTime;
            UpdateChargeUI();
        }

        if (Input.GetButtonUp("Fire1") && (isFirstFire || Time.time >= lastFireTime + fireDelay) && currentPlayerArrows > 0)
        {
            Fire();
            lastFireTime = Time.time;
            isFirstFire = false;
        }
    }

    void UpdateChargeUI()
    {
        float charge = Mathf.Clamp(currentChargeTime / maxChargeTime, 0f, 1f);
        chargeSlider.value = charge;
        float currentSpeed = Mathf.Lerp(0, arrowSpeed, charge);

        if (charge <= 0)
        {

        }
    }

    void Fire()
    {
        if (currentChargeTime <= 0 || currentPlayerArrows <= 0)
            return;

        GameObject arrowPrefabToUse;

        if (currentPlayerID == 1)
        {
            arrowPrefabToUse = arrowPrefabPlayer1;
            player1Arrows--; // ลดจำนวนลูกธนูของ Player 1
        }
        else
        {
            arrowPrefabToUse = arrowPrefabPlayer2;
            player2Arrows--; // ลดจำนวนลูกธนูของ Player 2
        }

        currentPlayerArrows--;

        if (arrowPrefabToUse != null && arrowSpawn != null)
        {
            GameObject arrow = Instantiate(arrowPrefabToUse, arrowSpawn.position, arrowSpawn.rotation);
            Rigidbody arrowRb = arrow.GetComponent<Rigidbody>();
            arrowRb.centerOfMass = new Vector3(0, 0, -0.5f);

            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Vector3 target;
            if (Physics.Raycast(ray, out hit))
            {
                target = hit.point;
            }
            else
            {
                target = ray.GetPoint(1000);
            }
            Vector3 direction = (target - arrow.transform.position).normalized;

            float charge = currentChargeTime / maxChargeTime;
            float currentSpeed = arrowSpeed * charge;
            arrowRb.velocity = direction * currentSpeed;

            ParticleSystem trail = Instantiate(arrowTrail, arrow.transform.position, Quaternion.identity);
            trail.transform.parent = arrow.transform;

            Destroy(arrow, 33.0f);
        }

        // อัปเดตจำนวนลูกธนูที่เหลือ
        UpdatePlayerArrowsText();
        // สลับผู้เล่นที่เล่นต่อไปหากผู้เล่นปัจจุบันไม่มีลูกธนูแล้ว
        if (currentPlayerArrows <= 0)
        {
            SwitchPlayerTurn();
        }
    }


    public void SwitchPlayerTurn()
    {
        currentPlayerID = (currentPlayerID == 1) ? 2 : 1;
        currentPlayerArrows = (currentPlayerID == 1) ? player1Arrows : player2Arrows;
        // แสดงภาพผู้เล่นที่สองหรือปิดตาม currentPlayerID
        StartCoroutine(ShowPlayerImageForCurrentTurn());
        UpdatePlayerArrowsText();
    }

    IEnumerator ShowPlayerImageForCurrentTurn()
    {
        yield return new WaitForSeconds(5f); // รอเวลา 3 วินาที

        UpdateTurnUI(); // แสดง UI ของผู้เล่นที่เปลี่ยนไป

        // เรียกใช้ฟังก์ชัน HidePlayerImage() เพื่อซ่อนภาพของผู้เล่นก่อนหน้า
        StartCoroutine(HidePlayerImage());

        // ปิดทันทีเมื่อทั้งสองฝ่ายเล่นเสร็จสิ้น
        if (player1Arrows <= 0 && player2Arrows <= 0) // เพิ่มเงื่อนไขนี้
        {
            player1Image.SetActive(false);
            player2Image.SetActive(false);
        }
    }

    void UpdateTurnUI()
    {
        if (currentPlayerID == 1)
        {
            player1Image.SetActive(true);
            player2Image.SetActive(false);
        }
        else if (currentPlayerID == 2)
        {
            player1Image.SetActive(false);
            player2Image.SetActive(true);
        }
    }

    IEnumerator HidePlayerImage()
    {
        yield return new WaitForSeconds(displayTime); // รอเวลาที่กำหนดในการแสดงผล

        // ปิดภาพผู้เล่นที่แสดงตาม currentPlayerID
        player1Image.SetActive(false);
        player2Image.SetActive(false);
    }

    void UpdatePlayerArrowsText()
    {
        // อัปเดตข้อความของจำนวนลูกธนูที่เหลือของแต่ละผู้เล่น
        player1ArrowsText.text = "" + player1Arrows.ToString();
        player2ArrowsText.text = "" + player2Arrows.ToString();
    }
}

